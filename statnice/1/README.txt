Moje státnicové otázky k zimnímu semestru 19/20
Které jsem si vypracoval na základě vyhledatelných zdrojů (google). Vesměs to byly VŠ skripta, učební texty pro elektrotechnické školy, wikipedia + pár dalších webů odbornějšího charakteru.
Snažil jsem se vypracovat otázky tak, aby se mně samotnému dobře učily. Některé jsou popsané líp, některé hůř a k pár jsem nic nedohledal.